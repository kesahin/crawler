#include "webcrawler.h"
#include <iostream>
#include <algorithm>

#define MAXCON 200
#define MAXREQ 500
#define MAXTOTAL 20000
#define MAXLINKPERPAGE 5

int relative_links = 0;

/*
 *This method aims to check given content is whether a valid html or not
 */
bool is_a_html(char *content_type)
{
    return content_type != nullptr && strlen(content_type) > 10 && strstr(content_type, "text/html");
}

/*
 * This method is responsible for increasing the buffer size of the data container that
 * obtaines the CURL responses.
 */
size_t increase_container_size(void *contents, size_t size, size_t nsize, void *ctx)
{
    size_t actual_size = size * nsize;
    WebCrawler::Container *t_container = (WebCrawler::Container *) ctx;
    char *t_buf = (char *)realloc(t_container->buf, t_container->size + actual_size);
    if (!t_buf)
    {
        std::cout << "Cannot reallocate with the additional size of " << actual_size << std::endl;
        return 0;
    }
    t_container->buf = t_buf;
    memcpy(&(t_container->buf[t_container->size]), contents, actual_size);
    t_container->size += actual_size;

    return actual_size;
}

/*
 * This method creates a CURL instance for a given url.
 * The options are set using the reference of The CURL library
 * via https://curl.se/libcurl/c/. (libcurl)
 */
CURL *WebCrawler::create_handler_for_url(char *url)
{
    CURL *handler_for_url = curl_easy_init();
    curl_easy_setopt(handler_for_url, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2TLS);
    curl_easy_setopt(handler_for_url, CURLOPT_URL, url);

    Container *container = (Container *)malloc(sizeof(Container));
    container->size = 0;
    container->buf = (char *)malloc(1);

    curl_easy_setopt(handler_for_url, CURLOPT_WRITEFUNCTION, increase_container_size);
    curl_easy_setopt(handler_for_url, CURLOPT_WRITEDATA, container);
    curl_easy_setopt(handler_for_url, CURLOPT_PRIVATE, container);

    curl_easy_setopt(handler_for_url, CURLOPT_ACCEPT_ENCODING, "");
    curl_easy_setopt(handler_for_url, CURLOPT_TIMEOUT, 5L);
    curl_easy_setopt(handler_for_url, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(handler_for_url, CURLOPT_MAXREDIRS, 10L);
    curl_easy_setopt(handler_for_url, CURLOPT_CONNECTTIMEOUT, 2L);
    curl_easy_setopt(handler_for_url, CURLOPT_COOKIEFILE, "");
    curl_easy_setopt(handler_for_url, CURLOPT_FILETIME, 1L);
    curl_easy_setopt(handler_for_url, CURLOPT_USERAGENT, "mini crawler");
    curl_easy_setopt(handler_for_url, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_easy_setopt(handler_for_url, CURLOPT_UNRESTRICTED_AUTH, 1L);
    curl_easy_setopt(handler_for_url, CURLOPT_PROXYAUTH, CURLAUTH_ANY);
    curl_easy_setopt(handler_for_url, CURLOPT_EXPECT_100_TIMEOUT_MS, 0L);

    return handler_for_url;
}

/*
 * This method is responsible for reading CURL response and forwarding
 * it to the HtmlParseAndCurlHandleCreate method.
 * The mechanism for reading is obtained from
 * via https://curl.se/libcurl/c/. (libcurl)
 */
void WebCrawler::triggerReading(char *base, char *relative)
{
    int messages_left;
    int still_running = 1;

    while(still_running)
    {
        int ret;
        curl_multi_wait(multi_handler, NULL, 0, 1000, &ret);
        curl_multi_perform(multi_handler, &still_running);

        CURLMsg *curl_message = nullptr;
        while((curl_message = curl_multi_info_read(multi_handler, &messages_left)))
        {
            if (curl_message->msg == CURLMSG_DONE)
            {
                CURL *handler = curl_message->easy_handle;
                char *url;
                Container *container;
                curl_easy_getinfo(handler, CURLINFO_PRIVATE, &container);
                curl_easy_getinfo(handler, CURLINFO_EFFECTIVE_URL, &url);

                if (curl_message->data.result == CURLE_OK)
                {
                    long result_status;
                    curl_easy_getinfo(handler, CURLINFO_RESPONSE_CODE, &result_status);
                    Node *node = new Node;
                    if (result_status == 200)
                    {
                        char *content_type;
                        curl_easy_getinfo(handler, CURLINFO_CONTENT_TYPE, &content_type);
                        if (is_a_html(content_type) && container->size > 100) {
                            HtmlParseAndCurlHandleCreate(multi_handler,container,url);
                        }
                    } else {
                        //Not implemented, but it can be used to differentiate
                        //for handling 40x,... other type of results from web.
                    }
                }
                else {
                    still_running = false;
                    break;
                }
                curl_multi_remove_handle(multi_handler, handler);
                curl_easy_cleanup(handler);
                free(container->buf);
                free(container);
                still_running = false;
            }
        }
    }
}

/*
 * This method is responsible for parsing the XML and analyzing
 * the relative paths, if incoming relative path is not requested
 * before, then it will be requested and inserted into map. On the
 * otherhand, it will be forwarded to insertion mechanism.
 * References:
 *      https://curl.se/libcurl/c/. (libcurl)
 *      http://www.xmlsoft.org/examples/
 */

void WebCrawler::HtmlParseAndCurlHandleCreate(CURLM *multihandler, WebCrawler::Container *container, char *url)
{
    int opts = HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR | \
            HTML_PARSE_NOWARNING | HTML_PARSE_NONET;
    htmlDocPtr doc = htmlReadMemory(container->buf, container->size, url, nullptr, opts);
    if(!doc)
        return;
    xmlChar *xpath = (xmlChar*) "//a/@href";
    xmlXPathContextPtr context = xmlXPathNewContext(doc);
    xmlXPathObjectPtr result = xmlXPathEvalExpression(xpath, context);
    xmlXPathFreeContext(context);
    if(!result)
        return;
    xmlNodeSetPtr nodeset = result->nodesetval;
    if(xmlXPathNodeSetIsEmpty(nodeset)) {
        xmlXPathFreeObject(result);
        return;
    }
    size_t count = 0;
    int i;
    for(i = 0; i < nodeset->nodeNr; i++) {
        double r = rand();
        int x = r * nodeset->nodeNr / RAND_MAX;
        const xmlNode *node = nodeset->nodeTab[x]->xmlChildrenNode;
        xmlChar *href = xmlNodeListGetString(doc, node, 1);
        if(relative_links) {
            xmlChar *orig = href;
            href = xmlBuildURI(href, (xmlChar *) url);
            xmlFree(orig);
        }
        char *link = (char *) href;
        if(!link || strlen(link) < 20)
            continue;

        if (strstr(link, "afiniti.com") == nullptr) {
            //To filter afiniti.com domain
            if (!strncmp(link,"/",1) && strncmp(link, "//", 2)) {
                //To filter relative paths only

                //To generate full url from relative path that will be
                //requested if it is not requested before.
                char *domain_url = "https://www.afiniti.com";
                char *full_url = (char *) malloc(strlen(domain_url) + strlen(link) + 1);
                strcpy(full_url,domain_url);
                char *slashed_removed_link;
                std::string surl = domain_url;
                if (surl.at(surl.size() - 1) == '/')
                    slashed_removed_link = link + 1;
                else
                    slashed_removed_link = link;
                strcat(full_url, slashed_removed_link);


                bool isThisLinkExist = false;
                if(relativePaths.find(link) != relativePaths.end())
                    isThisLinkExist = true;

                if (!isThisLinkExist) {
                    //creating handler for this generated url,
                    //Inserting it to the relativePaths map
                    //and invoking the insertToDs method.
                    curl_multi_add_handle(multi_handler, create_handler_for_url(full_url));
                    relativePaths.insert(std::make_pair(link,true));
                    insertToDs(url, link, full_url);
                    triggerReading(url, link);
                }
                else {
                    //No need to create a handler, since this generated
                    //url is requested before. Only requirement is to
                    //insert this url to insertedNodes map.
                    insertToDs(url, link, full_url);
                }
            }

        }
    }
}

/*
 * This method helps a visite node to be inserted into
 * insertedNodes map with respect to the Node struct
 *
 * VN: Visited Node at that request
 * P:  Parent  Node at that request
 * Cases
 *              Exists                Not-Exists
 *                -                      V/P
 *                P                       V
 *                V                       P
 *                V/P                     -
 */
void WebCrawler::insertToDs(char *base, char *relative, char *full)
{
    //Creating a node that will be [inserted into insertedNodes map | modified for depth&parent]
    Node *node = new Node;

    if (insertedNodes.find(full) == insertedNodes.end())
    {
        //If this node is not an element of insertedNodes map
        node->url = relative;
        node->nodeDepth = 0;
        if (insertedNodes.find(base) == insertedNodes.end())
        {
            //If the parent at this request is not an element of insertedNodes map
            //Creating parent
            Node* parent_node = new Node;
            parent_node->url = base;
            parent_node->parent = nullptr;
            parent_node->nodeDepth = 0;

            node->parent = parent_node;
            node->url = relative;
            node->nodeDepth++;
            parent_node->childNodes.insert(std::make_pair(full,node));
            //Inserting the nodes to insertedNodes map
            insertedNodes.insert(std::make_pair(base, parent_node));
            insertedNodes.insert(std::make_pair(full, node));
        }
        else
        {
            //If the parent at this request is an element of insertedNodes map
            node->parent = insertedNodes[base];
            node->nodeDepth++;

            Node* d_node = insertedNodes[base];
            //Finding the node depth
            while(d_node->parent != nullptr)
            {
                d_node = d_node->parent;
                node->nodeDepth++;
            }

            //Inserting the nodes to insertedNodes map
            insertedNodes[base]->childNodes.insert(std::make_pair(full,node));
            insertedNodes.insert(std::make_pair(full, node));
        }
    }
    else
    {
        //If this node is an element of insertedNodes map
        if (insertedNodes.find(base) == insertedNodes.end()) {
            //If the parent at this request is not an element of insertedNodes map
            Node* parent_node = new Node;
            parent_node->url = base;
            parent_node->parent = nullptr;
            parent_node->nodeDepth = 0;
            parent_node->childNodes.insert(std::make_pair(full,insertedNodes[full]));

            //Inserting the nodes to insertedNodes map
            insertedNodes.insert(std::make_pair(base,parent_node));
        }
        else
        {
            //If the parent at this request is an element of insertedNodes map
            int depth = 1;

            Node* d_node = insertedNodes[base];
            //Finding the node depth
            while(d_node->parent != nullptr)
            {
                d_node = d_node->parent;
                depth++;
            }
            //Updating node depth, parent to adjust
            //depth comparisons for next iterations
            if (depth < insertedNodes[full]->nodeDepth)
            {
                insertedNodes[full]->nodeDepth = depth;
                insertedNodes[full]->parent = insertedNodes[base];
                insertedNodes[base]->childNodes.insert(std::make_pair(full,insertedNodes[full]));
            }
        }
    }
}

/*
 * This method rearranges the depth of each
 * node with respect to the parent nodes
 */
void WebCrawler::rearrangeNodes()
{
    std::map<std::string, Node*>::iterator it;

    for (it = insertedNodes.begin(); it != insertedNodes.end(); it++)
    {
        int depth = 0;
        if (it->second->parent != nullptr)
        {
            Node *node = it->second;
            while(node->parent != nullptr)
            {
                node = node->parent;
                depth++;
            }
            it->second->nodeDepth = depth;
        }
    }
}

/*
 *Constructor of WebCrawler
 *"Initialization of curl instance"
 *curl_multi_setopt is used to configure this instance
 *Creating base "https://www.afiniti.com/" node
 *Then to trigger the information gathering for this web-site.
 */
WebCrawler::WebCrawler()
{
    curl_global_init(CURL_GLOBAL_DEFAULT);
    multi_handler = curl_multi_init();

    curl_multi_setopt(multi_handler, CURLMOPT_MAX_TOTAL_CONNECTIONS, MAXCON);
    curl_multi_setopt(multi_handler, CURLMOPT_MAX_HOST_CONNECTIONS, 6L);
    curl_multi_setopt(multi_handler, CURLMOPT_PIPELINING, CURLPIPE_MULTIPLEX);

    char *initial_page = "https://www.afiniti.com/";
    curl_multi_add_handle(multi_handler, create_handler_for_url(initial_page));

    Node *node = new Node;
    node->parent = nullptr;
    node->url = initial_page;
    node->nodeDepth = 0;
    insertedNodes.insert(std::make_pair(initial_page, node));

    triggerReading(initial_page, "https://www.afiniti.com/");

    std::map<std::string, Node*>::iterator it = insertedNodes.begin();
    Node *t_node = it->second;

    rearrangeNodes();

    printInsertedNodes(t_node);
}

/*
 *This method prints the obtained node information
 *in a recursive manner
 */
void WebCrawler::printInsertedNodes(Node *node, int depth)
{
    std::map<std::string, Node*>::iterator it;
    if (node->parent == nullptr)
        std::cout << "base link: " << node->url << std::endl;

    depth++;
    for (it = node->childNodes.begin(); it != node->childNodes.end(); it++)
    {
        for (int i = 0; i < depth; i++)
            std::cout << "\t";
        std::cout << "- " << it->second->url << std::endl;
        if (depth <= it->second->nodeDepth)
            printInsertedNodes(it->second, depth);
    }
}

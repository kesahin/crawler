#ifndef WEBCRAWLER_H
#define WEBCRAWLER_H

#include <curl.h>
#include <libxml/HTMLparser.h>
#include <libxml/xpath.h>
#include <libxml/uri.h>
#include <string.h>
#include <map>

///
/// \brief The WebCrawler class
/// This class aims to browse the https://afiniti.com/
/// internet page and to index the content of relative paths
/// of this website.
class WebCrawler
{
public:

    struct Container {
        char *buf;
        size_t size;
    };

    /*Node data structure represents a url address that has
     *a parent url address(parent), relative path name(url),
     *url addresses(childNodes) that are reached via this node
     *and the depth of this node in terms of hop to the https://www.afiniti.com/
     *domain.
     */
    struct Node {
        Node *parent;
        std::string url;
        std::map<std::string, Node*> childNodes;
        int nodeDepth = 0;
    };

    WebCrawler();
    void printInsertedNodes(Node *node, int depth = 0);
protected:
    CURL *create_handler_for_url(char *url);
    void triggerReading(char *base, char *relative);
    void HtmlParseAndCurlHandleCreate(CURLM *multihandler, Container *container, char *url);
    void insertToDs(char *base, char *relative, char *full);
    void rearrangeNodes();

private:
    CURLM *multi_handler;
    //insertedNodes contains the information of nodes that are obtained while crawling
    std::map<std::string,Node*> insertedNodes;
    //relativePaths prevents to crawl same nodes again.
    std::map<std::string,bool> relativePaths;
};

#endif // WEBCRAWLER_H

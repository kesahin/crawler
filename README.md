# Crawler

A Sample Web Crawler C++ Implementation

This project aims to search relative paths on https://www.afiniti.com/.
To achieve this mission CURL(libcurl) and XML(libxml2) libraries are used
in this project.

# Distro/GCC
- Distro:Ubuntu 20.04
- GCC: 9.3.0

# Dependencies
- libcurl
- libxml2

# Install libcurl

- sudo apt install libcurl4
- sudo apt install libcurl4-openssl-dev
- sudo apt install libcurl4-gnutls-dev

# Install libxml2
sudo apt install libxml2-dev

# Compile with GCC
g++ -Wall -g webcrawler.cpp main.cpp -I/usr/include/x86_64-linux-gnu/curl -I/usr/include/libxml2 -lcurl -lxml2 -o webcrawler

# Compile with QT
Open this project on QTCreator usin WebCrawler.pro file,
Specify the build directory then 
- qmake
- build
- run

# Run with Docker
Run with Docker
- docker pull kesahin/afcrawler:latest
- docker run kesahin/afcrawler



